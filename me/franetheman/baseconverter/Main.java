package me.franetheman.baseconverter;

import java.util.Scanner;

/**
 * Created by frans on 26/06/2016.
 */
public class Main
{

    public static void main(String[] args)
    {
        Converter c = new Converter();
        Scanner sc = new Scanner(System.in);

        String num;
        int numBase;
        int newBase;

        System.out.print("Enter a number in any base between 2 and 35! ");
        num = sc.next();


        System.out.print("Enter the numbers base! ");
        numBase = sc.nextInt();

        System.out.print("Enter the base to convert to! ");
        newBase = sc.nextInt();

        System.out.println("================================");
        System.out.println(c.convertBaseNToBaseN(num, numBase, newBase));

        sc.close();
    }

}
