package me.franetheman.baseconverter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by frans on 26/06/2016.
 */
public final class ConversionUtil
{

    private static Map<Character, Integer> charValues = new HashMap<>();

    static
    {
        charValues.put('0', 0);   charValues.put('1', 1);   charValues.put('2', 2);
        charValues.put('3', 3);   charValues.put('4', 4);   charValues.put('5', 5);
        charValues.put('6', 6);   charValues.put('7', 7);   charValues.put('8', 8);
        charValues.put('9', 9);
        charValues.put('A', 10);   charValues.put('B', 11);   charValues.put('C', 12);
        charValues.put('D', 13);   charValues.put('E', 14);   charValues.put('F', 15);
        charValues.put('G', 16);   charValues.put('H', 17);   charValues.put('I', 18);
        charValues.put('J', 19);   charValues.put('K', 20);   charValues.put('L', 21);
        charValues.put('M', 22);   charValues.put('N', 23);   charValues.put('O', 24);
        charValues.put('P', 25);   charValues.put('Q', 26);   charValues.put('R', 27);
        charValues.put('S', 28);   charValues.put('T', 29);   charValues.put('U', 30);
        charValues.put('V', 31);   charValues.put('W', 32);   charValues.put('X', 33);
        charValues.put('Y', 34);   charValues.put('Z', 35);
    }

    public static boolean isValid(String num, int numBase, int newBase)
    {
        num = num.toUpperCase();

        if(numBase <= 1 || newBase <= 1)
        {
            return false;
        }

        if(numBase > 35 || newBase > 35)
        {
            return false;
        }

        for(char c : num.toCharArray())
        {
            if(charValues.get(c) >= numBase)
                return false;
        }

        return true;
    }

    public static int toDigit(Character c)
    {
        c = c.toString().toUpperCase().charAt(0);
        if(charValues.containsKey(c))
            return charValues.get(c);

        else
            return 0;
    }

    public static char toChar(int i)
    {
        for(Map.Entry<Character, Integer> e : charValues.entrySet())
        {
            if(e.getValue() == i)
            {
                return e.getKey();
            }
        }

        return ' ';
    }
}
