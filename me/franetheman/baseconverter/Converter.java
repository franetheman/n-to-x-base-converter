package me.franetheman.baseconverter;

/**
 * Created by frans on 26/06/2016.
 */
public class Converter
{

    public Converter()
    {

    }

    public String convertBaseNToBaseN(String num, int numBase, int newBase)
    {
        if(!ConversionUtil.isValid(num, numBase, newBase))
        {
            System.err.println("Illegal input!");
            return null;
        }

        num.toUpperCase();

        char[] characters = num.toCharArray();
        int decimal = 0;
        int pow = 0;

        for(int i = characters.length - 1 ; i >= 0 ; i--)
        {
            decimal += ConversionUtil.toDigit(characters[i]) * Math.pow((double)numBase, pow);

            pow++;
        }

        String result = "";

        for(;;)
        {
            result = ConversionUtil.toChar(decimal % newBase) + result;
            decimal /= newBase;

            if(decimal / newBase == 0)
            {
                result = ConversionUtil.toChar(decimal % newBase) + result;
                break;
            }
        }

        return result;
    }

}
